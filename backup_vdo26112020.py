from PIL import Image
from TOOLS import Functions
import argparse
import math
import numpy as np
import cv2
import imutils

cap = cv2.VideoCapture(0)
print ("press q to exit")
counter = 0

while(True):
    # Capture frame-by-frame
    ret, frame = cap.read()
    # display original frame
    cv2.imshow('original',frame)

    # Our operations on the frame come here
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    # Display the resulting gray frame
    #cv2.imshow('gray vdo',gray)

    # Edge detect processing
    edged = cv2.Canny(gray,30,200)
    cv2.imshow('edged',edged)

    #cnts = cv2.findContours(edged, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
    #cnts = imutils.grab_contours(cnts)
    #cnts = sorted(cnts, key = cv2.contourArea, reverse = True)[:10]
    #screenCnt = None

    #for c in cnts:
    #    peri = cv2.arcLength(c, True)
    #    approx = cv2.approxPolyDP(c, 0.018 * peri, True)
  
    #   if len(approx) == 4:
    #       print(len(approx), ":",approx)
    #        screenCnt = approx
    #        break
    #print("screanCnt",screenCnt)
    
    #mask = np.zeros(gray.shape,np.uint8)
    #new_image = cv2.drawContours(frame,[screenCnt],0,255,-1,)
    #new_image = cv2.bitwise_and(frame,frame,mask=mask)
    
    #cv2.imshow("new Image",new_image)
##############################################################
    contours, heirarchy = cv2.findContours(edged, cv2.RETR_TREE, cv2.CHAIN_APPROX_NONE)

    cnts = contours[0]
    screenCnt = None

    for contour in contours:
        # get rectangle bounding contour
        [x,y,w,h] = cv2.boundingRect(contour)

    # draw rectangle around contour on original image
    cv2.rectangle(frame,(x,y),(x+w,y+h),(255,0,255),2)

    ##### code added..for thresholding
    for c in cnts:
        # approximate the contour
        peri = cv2.arcLength(c, True)
        approx = cv2.approxPolyDP(c, 0.018 * peri, True)

        # if our approximated contour has four points, then
        # we can assume that we have found our screen
        if len(approx) == 4:
            screenCnt = approx
            break

    mask = np.zeros(gray.shape,np.uint8)
    new_image = cv2.drawContours(mask,[screenCnt],0,255,-1,)
    new_image = cv2.bitwise_and(frame,frame,mask=mask)

    cv2.imshow("new Image",new_image)


    
##############################################################
    contours, hierarchy = cv2.findContours(edged, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE) 
  
    #cv2.imshow('Canny Edges After Contouring', edged) 
    #cv2.waitKey(0) 
  
    print("Number of Contours found = " + str(len(contours))) 
  
    # Draw all contours 
    # -1 signifies drawing all contours 
    cv2.drawContours(frame, contours, -1, (0, 255, 0), 3) 
  
    cv2.imshow('Contours', frame)
    #cv2.waitKey(0) 
##############################################################
    if cv2.waitKey(1) & 0xFF == ord('q'):
        print ("Ending program")
        break

# When everything done, release the capture
cap.release()
cv2.destroyAllWindows()